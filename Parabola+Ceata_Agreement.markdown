% Fiscal Sponsorship Agreement
% Parabola GNU/Linux-libre and Ceata Foundation
% July, 2015

This Agreement is made by and between:

1. Fundația Ceata ("Ceata") represented by President Tiberiu-Constantin
   Turbureanu, and

2. Nicolás Reynolds on behalf of the project known as Parabola
   GNU/Linux-libre ("Parabola")

each, a "Party"; together, "the Parties". Ceata is a foundation
(nonprofit organization) incorporated in Bucharest, Romania, and
exempted from income tax under Romanian law 246/2005.

Whereas:

a) Ceata's organizational mission is to liberate users from restrictions
in art and technology, with Free Software and Free Culture.  "Free
Software" is software its users are free to use, redistribute, modify
and distribute modified versions. Similarly, "Free Culture" is any
cultural work the public is free to use, redistribute, modify and
distribute modified versions.

b) Parabola's purpose is to develop and maintain a bleeding-edge 100%
Free Software and Free Culture distribution of the GNU operating system.
Parabola is recognized by the Free Software Foundation as a system
distribution that respects the Free System Distribution Guidelines and
is thus a 100% Free System Distribution.

c) Ceata desires to act as the fiscal sponsor of Parabola beginning on
the Effective Date (as defined below) to assist Parabola in
accomplishing its purpose, which Ceata has determined is fully aligned
with Ceata's mission.

d) Ceata's Council has approved the establishment of a fund ("Parabola
Fund")  to receive donations of cash and other property earmarked for
support of Parabola and to make disbursements in furtherance of
Parabola.

Now, therefore, the Parties hereby agree as follows:

1. **Term of Agreement.** As of the Effective Date, Parabola benefits
   from the fiscal sponsorship of Ceata for a term of 1 year, unless and
   until terminated as set forth in **§ 7**.  The term can be extended
   definitely or indefinitely by an amendment to this Agreement, signed
   by the Parties at least 60 days prior to the expiration of the 1-year
   term.

2. **Parabola's Activities and Communication of All Donation-related
   Decisions.**

   a) **Nicolás Reynolds is Parabola's Delegate** (the "Delegate") to
   communicate any and all donation-related (including but not limited
   to expenditure) decisions to Ceata's Council.  Ceata will only object
   to the decisions to the extent Parabola is not in compliance with §
   2(b) or § 5 of this Agreement, or when the Delegate's communication
   isn't in conjuction with a Parabola document or resource at
   parabola.nu website.


   b) **Parabola stays Free Software and Free Culture.** Ceata and
   Parabola's Delegate agree that any and all software and/or
   documentation/cultural works distributed by Parabola will be
   distributed solely as Free Software and Free Culture, respectively.
   Acceptable exceptions to Free Culture works are non-modifiable
   license texts and works under the GNU Free Documentation License with
   invariant sections, as well as works of opinion.  Ceata retains the
   sole right to determine whether Parabola's software and/or
   documentation/cultural works constitute Free Software and Free
   Culture, respectively (as defined herein).

   c) **Ultimate Responsibility of the Parties.** Subject to **§ 2(a)**
   of this Agreement, any and all donation-related information,
   processing and acknowledgement of cash and non-cash revenue items,
   accounts payable and receivable, negotiation of leases and contracts
   and disbursement of Parabola funds (including grants) shall be the
   ultimate responsibility of Ceata and shall be conducted in the name
   of Ceata, beginning of the Effective Date.

   d) **Parabola Not An Agent of Ceata.** The Parties acknowledge that
   Parabola and its Delegate do not and shall not act as an agent for
   Ceata unless specifically authorized in writing by Ceata to do so.

3. **Fees.**

   a) Ceata's Council has agreed the fiscal sponsorship offered Parabola
   does Not oblige Parabola to donate to Ceata a percentage of
   Parabola's gross revenue, for Ceata's operational costs.

   b) Notwithstanding the above, the Parties agree that should Ceata be
   required to pay any taxes (including but not limited to sales taxes
   and unrelated business taxable income) as the result of any activity
   of Parabola and/or activities undertaken by Ceata on Parabola's
   behalf, such taxes shall be deducted from the Parabola Fund.  Such
   deductions from the Parabola Fund will be made by Ceata within
   fifteen (15) days of said activities and the Delegate will be
   notified when the taxes are payed.

   c) Ceata will monitor any unrelated business taxable income and may
   require Parabola to cease activities generating such income if the
   overall amounts exceed amounts permissible or prudent for Ceata,
   given Ceata's tax exempt status.

4. **Parabola Fund/Variance Power.** Beginning on the Effective Date,
   Ceata shall place all gifts, grants, contributions and other revenues
   received by Ceata and identified with Parabola into a Parabola Fund
   to be used as communicated by Parabola's Delegate to accomplish
   Parabola’s purpose (as defined herein).  Ceata retains the right to
   refuse donation-related (including but not limited to expenditure)
   decisions to the extent Parabola is not in compliance with §2(b) or
   §5 of this Agreement.  The Parties agree that all money, and the fair
   market value of all property, deposited in the Parabola Fund be
   reported as the income of Ceata, for both tax purposes and for
   purposes of Ceata’s financial statements.  It is the intent of the
   Parties that this Agreement be interpreted to provide Ceata with
   variance powers necessary to enable Ceata to treat the Parabola Fund
   as Ceata’s asset while this Agreement is in effect.

5. **Parabola Fund Management / Performance of Tax-exempt Purposes.**
   All of the assets received by Ceata under the terms of this Agreement
   shall be devoted to the purpose of Parabola, within the tax-exempt
   purposes of Ceata.  The Parties agree not to use its funds or operate
   in any way which would jeopardize the tax-exempt status of Ceata.

6. **Outstanding Liabilities.**  The signatories represent that any
   liabilities that may be outstanding in connection with Parabola have
   been disclosed to Ceata.

7. **Termination.**  Parabola's Delegate or Ceata may terminate this
   Agreement at any time before its term ends subject to the
   understandings below.

   a) **Notice and Fiscal Sponsor Successor Search.**  Either Parabola's
   Delegate or Ceata may terminate this Agreement on sixty (60) days
   written notice ("the Notice Period") to the other Party, so long as a
   Successor can be found that meets the following requirements (the
   "Successor has Qualified"):

   1. The Fiscal Sponsor Successor is another nonprofit organization
      which is tax-exempt,

   2. the Fiscal Sponsor Successor is willing and able to sponsor
      Parabola,

   3. the Fiscal Sponsor Successor has **(a)** communicated its
      willingness to sponsor Parabola in writing to Ceata and **(b)**
      sent a copy of its incorporation documents to Ceata, and,

   4. the Fiscal Sponsor Successor is approved in writing by both
      Parties by the end of the Notice Period, such approval not to be
      unreasonably withheld.

   b) **Transfer to a Fiscal Sponsor Successor.**  If a Fiscal Sponsor
   Successor has Qualified, the balance of assets in Parabola Fund,
   together with any other assets held or liabilities incurred by Ceata
   in connection with Parabola, shall be transferred to the Fiscal
   Sponsor Successor within thirty (30) days of the approval of the
   Fiscal Sponsor Successor in writing by both Parties or any extension
   thereof, subject to the approval of any third parties that may be
   required.

   c) **Termination Without a Fiscal Sponsor Successor.**  If no Fiscal
   Sponsor Successor is found, the Parabola Fund will remain inactive
   for any motive other than transference to a Fiscal Sponsor Successor.
   Neither Ceata nor Parabola's Delegate will be able to utilize its
   funds until a Successor is available under the conditions set by
   **§7(a)** and transferred to the Qualified Successor according to
   **§7(b)**.

8. **Parabola Delegate Succession.** In the case of the Delegate's
   inability to comply with **§2(a)** or **§7** of this Agreement, this
   Agreement will be immediately terminated without preclusion of a new
   Agreement between Ceata and a Parabola's Delegate.  If no Parabola
   Delegate Succession is possible within the same time period specified
   by **§7(a)**, the Parabola Fund will be handled as per **§7(c)**.

9. **Miscellaneous.**  Each provision of this Agreement shall be
   separately enforceable, and the invalidity of one provision shall not
   affect the validity or enforceability of any other provision.  This
   Agreement shall be interpreted and construed in accordance with the
   laws of Romania.  This Agreement constitutes the only agreement, and
   supersedes all prior agreements and understandings, both written and
   oral, among the Parties with respect to the subject matter hereof.

10. **Amendments.**  This Agreement may not be amended or modified,
    except in writing and signed by both Ceata and Parabola's Delegate.

11. **Counterparts / Facsimile.**  This Agreement may be executed in two
    or more counterparts, each of which shall constitute an original,
    but all of which, when together, shall constitute but one and the
    same instrument, and shall become effective when one or more
    counterparts have been signed by each Party hereto and delivered to
    the other Party. In lieu of the original, a facsimile transmission
    or copy of the original shall be as effective and enforceable as the
    original.

In witness whereof, the Parties have executed this Fiscal Sponsorship
Agreement effective on the 15 day of July, 2015 (the "Effective Date").

-----------------------------   ----------------
By:                             By:
Date:                           Date:
Fundația Ceata                  Nicolás Reynolds
Tiberiu-Constantin Turbureanu
Title: President
-----------------------------   ----------------
