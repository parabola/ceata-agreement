default: all

files = $(wildcard *.markdown)
pdfs = $(patsubst %.markdown, %.pdf, $(files))

all: $(pdfs)

clean:
	rm -f $(pdfs)

%.pdf: %.markdown
	pandoc -o "$@" -V fontsize=12pt,a4paper \
	               -V documentclass=article \
	               -V geometry=hcentering \
	               --latex-engine=xelatex \
	               "$<"
